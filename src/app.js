import Alpine from 'alpinejs'
import router from '@shaun/alpinejs-router'
import './app.css'

Alpine.plugin(router)

window.Alpine = Alpine

Alpine.store('shop', {
  name: 'Alpine-Shop',
  products: ['Swiss Alp Chocolate', 'Car Alpine A22110'],
  search: '',
  fopen: false,
  posts: [{ title: 'post 1' }, { title: 'post 2' }, { title: 'post 3' }, { title: 'post 4' }],
})

Alpine.start()
